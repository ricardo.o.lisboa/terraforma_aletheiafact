terraform {
  required_version = "~> 1.1.0" #all more versions 1.1.0

#add servers

  required_providers {
    google = {
      version = ">= 3.0.0"
      source = "hashicorp/google"
    }
  }
}


#add providers
provider "google" {
  credentials = file("//home/ricardo/terraforma_aletheiafact/key/terraform-teste-30052022-dadceb285b31.json")
  project     = "terraform-teste-30052022"
  region      = "us-central1"
}

resource "google_compute_network" "vpc_network" {
  name                    = "main"
  auto_create_subnetworks = true
}

module "vpc" {

}

variable "ip" {
  
}