#variables bucket
variable "bucket_name" {
  type        = string
  description = "aletheianewtest001"
}
variable "bucket_location" {
  type    = string
  default = "us-central1"
}
variable "project_id" {
  type    = string
  default = "terraform-teste-30052022"
}
variable "storage_class" {
  type    = string
  default = "REGIONAL"

}

#variables credentials
